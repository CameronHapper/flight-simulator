/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RefineryUtilities;

import asgn2Aircraft.AircraftException;
import asgn2Passengers.PassengerException;


/**
 * @author hogan
 *
 */
@SuppressWarnings("serial")
public class GUISimulator extends JFrame implements ActionListener, Runnable {
	
	private Simulator sim;
	private Log log;
	private Flights flights;
	
	private TimeSeriesCollection dataset;
	private TimeSeriesCollection datasetBookingRejected;
	private JTextArea textArea;
	private JScrollPane sp;
	
	private JFreeChart chart;
	
	private JTextField rngSeed;
	private JTextField dailyMean;
	private JTextField queueSize;
	private JTextField cancellation;
	private JTextField first;
	private JTextField business;
	private JTextField premium;
	private JTextField economy;
	
	private JButton runSim;
	private JButton showLog;
	private JButton showChart;
	
	private String output;
	
	public static int rngSeedV;
	public static double dailyMeanV;
	public static int queueSizeV;
	public static double cancellationV;
	public static double firstV;
	public static double businessV;
	public static double premiumV;
	public static double economyV;
	public static double sd;
	
	//private JFrame frame;
	
	/**
	 * @param arg0
	 * @throws HeadlessException
	 */
	public GUISimulator(String arg0) throws HeadlessException {
		super(arg0);
		dataset = new TimeSeriesCollection();
		datasetBookingRejected = new TimeSeriesCollection();
		
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(new GridBagLayout());
        
        
        GridBagConstraints gbc = new GridBagConstraints();

		gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.insets = new Insets(10, 10, 10, 10);
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 4;
		
        chart  = createBookingChart(dataset);
        this.add(new ChartPanel(chart), gbc);
       
        

        
        gbc = new GridBagConstraints(); 
        gbc.insets = new Insets(0, 100, 0, 0);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        gbc.gridy = 1;
        gbc.gridx = 1;
        this.add(new JLabel("Simulators"), gbc);
        
        //RNG Seed
        gbc.gridy++;
        gbc.insets = new Insets(0, -65, 10, 0);
        this.add(new JLabel("RNG Seed"), gbc);
        gbc.insets = new Insets(0, 100, 10, 0);
        rngSeed = new JTextField("100");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(rngSeed, gbc);
        
        //Daily Mean
        gbc.gridy++;
        gbc.insets = new Insets(0, 20, 10, 0);
        this.add(new JLabel("Daily Mean"), gbc);
        gbc.insets = new Insets(0, 100, 10, 0);
        dailyMean = new JTextField("1300");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(dailyMean, gbc);
        
        //Queue Size
        gbc.gridy++;
        gbc.insets = new Insets(0, 20, 10, 0);
        this.add(new JLabel("Queue Size"), gbc);
        gbc.insets = new Insets(0, 100, 10, 0);
        queueSize = new JTextField("500");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(queueSize, gbc);
        
        //Cancellation
        gbc.gridy++;
        gbc.insets = new Insets(0, 20, 10, 0);
        this.add(new JLabel("Cancellation"), gbc);
        gbc.insets = new Insets(0, 100, 10, 0);
        cancellation = new JTextField("0.10");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(cancellation, gbc);
        
        gbc = new GridBagConstraints(); 
        gbc.insets = new Insets(0, 150, 0, 0);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        gbc.gridy = 1;
        gbc.gridx = 2;
        this.add(new JLabel("Fare Classes"), gbc);
        
        //First
        gbc.gridy++;
        gbc.insets = new Insets(0, -28, 10, 0);
        this.add(new JLabel("First"), gbc);
        gbc.insets = new Insets(0, 150, 10, 0);
        first = new JTextField("0.03");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(first, gbc);
        
        //Business
        gbc.gridy++;
        gbc.insets = new Insets(0, 85, 10, 0);
        this.add(new JLabel("Business"), gbc);
        gbc.insets = new Insets(0, 150, 10, 0);
        business = new JTextField("0.14");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(business, gbc);
        
        //Premium
        gbc.gridy++;
        gbc.insets = new Insets(0, 85, 10, 0);
        this.add(new JLabel("Premium"), gbc);
        gbc.insets = new Insets(0, 150, 10, 0);
        premium = new JTextField("0.13");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(premium, gbc);
        
        //Economy
        gbc.gridy++;
        gbc.insets = new Insets(0, 85, 10, 0);
        this.add(new JLabel("Economy"), gbc);
        gbc.insets = new Insets(0, 150, 10, 0);
        economy = new JTextField("0.70");
        gbc.fill = GridBagConstraints.HORIZONTAL;
        this.add(economy, gbc);
        
        
        gbc = new GridBagConstraints(); 
        gbc.insets = new Insets(0, 50, 0, 0);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.gridwidth = 1;
        gbc.gridy = 1;
        gbc.gridx = 3;
        this.add(new JLabel("Operation"), gbc);
        
        gbc = new GridBagConstraints(); 
        gbc.insets = new Insets(10, 50, 0, 10);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 1;
        gbc.gridy = 2;
        gbc.gridx = 3;
        runSim = new JButton("Run Simulation");
        runSim.addActionListener(this);
        this.add(runSim, gbc);
        
        gbc = new GridBagConstraints(); 
        gbc.insets = new Insets(0, 50, 0, 10);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 1;
        gbc.gridy = 4;
        gbc.gridx = 3;
        showChart = new JButton("Show Chart 2");
        showChart.addActionListener(this);
        showChart.setEnabled(false);
        this.add(showChart, gbc);
        
        gbc = new GridBagConstraints(); 
        gbc.insets = new Insets(10, 50, 10, 10);
        gbc.anchor = GridBagConstraints.CENTER;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = 1;
        gbc.gridy = 3;
        gbc.gridx = 3;
        showLog = new JButton("Show Log");
        showLog.addActionListener(this);
        this.add(showLog, gbc);
        showLog.setEnabled(false);
        
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
        
        

        
                
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		 
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame.setDefaultLookAndFeelDecorated(true);
		SwingUtilities.invokeLater(new GUISimulator("Test"));
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		Object src=arg0.getSource();
		
		if (src==runSim){
			dataset.removeAllSeries();
			datasetBookingRejected.removeAllSeries();
			rngSeedV = Integer.parseInt(rngSeed.getText());
			dailyMeanV = Double.valueOf(dailyMean.getText());
			queueSizeV = Integer.parseInt(queueSize.getText());
			cancellationV = Double.valueOf(cancellation.getText());
			firstV = Double.valueOf(first.getText());
			businessV = Double.valueOf(business.getText());
			premiumV = Double.valueOf(premium.getText());
			economyV = Double.valueOf(economy.getText());
			sd = 0.33 * GUISimulator.dailyMeanV;
			
			try {
				sim = new Simulator();
			} catch (SimulationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				log = new Log();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				runSimulation();
				
			} catch (AircraftException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PassengerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SimulationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
	        
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
	        gbc.gridy = 0;
	        gbc.insets = new Insets(10, 10, 10, 10);
	        gbc.fill = GridBagConstraints.HORIZONTAL;
	        gbc.gridwidth = 4;
			chart  = createBookingChart(dataset);
	        this.add(new ChartPanel(chart), gbc);
	        this.pack();
	        this.setVisible(true);
			
	        //Show chart 2 enable
			this.showChart.setEnabled(true);
			this.showLog.setEnabled(true);
			
		}else if (src == showChart){
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
	        gbc.gridy = 0;
	        gbc.insets = new Insets(10, 10, 10, 10);
	        gbc.fill = GridBagConstraints.HORIZONTAL;
	        gbc.gridwidth = 4;
	        chart  = createBookRefusedChart(datasetBookingRejected);
	        this.add(new ChartPanel(chart), gbc);
	        this.pack();
	        this.setVisible(true);
			
			
		}else if (src == showLog){
			
			JFrame logFrame = new JFrame("Log");
			logFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			logFrame.setLocationRelativeTo(null);
	        textArea = new JTextArea(20, 60);//Graph Area
	        textArea.setEditable(false);//
	        textArea.setBorder(BorderFactory.createEtchedBorder());//
	        textArea.setBackground(Color.WHITE);//
	        textArea.setText(output);
	        sp = new JScrollPane(textArea);
	        logFrame.add(sp, BorderLayout.CENTER);
	        logFrame.pack();
	        logFrame.setVisible(true);
	        
		}
		
		
	}
	
	public void runSimulation() throws AircraftException, PassengerException, SimulationException, IOException {
		
		TimeSeries firstTotal = new TimeSeries("First");
		TimeSeries busTotal = new TimeSeries("Business");
		TimeSeries premTotal = new TimeSeries("Premium");
		TimeSeries econTotal = new TimeSeries("Economy"); 
		TimeSeries bookTotal = new TimeSeries("Total Bookings");
		TimeSeries seatAvail = new TimeSeries("Available Seats");
		TimeSeries Queue = new TimeSeries("Passengers in Queue");
		TimeSeries RejectedPassenger = new TimeSeries("Rejected Passengers");

		//Base time, data set up - the calendar is needed for the time points
		Calendar cal = GregorianCalendar.getInstance();

		
		
		this.sim.createSchedule();
		this.log.initialEntry(this.sim);
		output = "Start of Simulation \n";
		//Main simulation loop 
		for (int time=0; time<=Constants.DURATION; time++) {
			this.sim.resetStatus(time); 
			output += time + ":";
			this.sim.rebookCancelledPassengers(time); 
			this.sim.generateAndHandleBookings(time);
			this.sim.processNewCancellations(time);
			if (time >= Constants.FIRST_FLIGHT) {
				flights = this.sim.getFlights(time);
				this.sim.processUpgrades(time);
				this.sim.processQueue(time);
				this.sim.flyPassengers(time);
				this.sim.updateTotalCounts(time); 
				this.log.logFlightEntries(time, sim);
			} else {
				
				this.sim.processQueue(time);
			}
			//Log progress 
			this.log.logQREntries(time, sim);
			this.log.logEntry(time,this.sim);
			boolean flying = (time >= Constants.FIRST_FLIGHT);
			output += sim.getSummary(time, flying);
			
			//Making Graph data
			cal.set(2016, 0, time);
			Date timePoint = cal.getTime();
			
			firstTotal.add(new Day(timePoint), sim.getTotalFirst());
	        busTotal.add(new Day(timePoint), sim.getTotalBusiness());
	        premTotal.add(new Day(timePoint), sim.getTotalPremium());
			econTotal.add(new Day(timePoint), sim.getTotalEconomy());
			bookTotal.add(new Day(timePoint), sim.getTotalFlown());
			seatAvail.add(new Day (timePoint), sim.getTotalEmpty());
			Queue.add(new Day(timePoint), sim.numInQueue());
			RejectedPassenger.add(new Day(timePoint), sim.numRefused());
		}
		//Collecting all of the data
		dataset.addSeries(firstTotal);
		dataset.addSeries(busTotal);
		dataset.addSeries(premTotal);
		dataset.addSeries(econTotal);
		dataset.addSeries(bookTotal);
		dataset.addSeries(seatAvail);
		datasetBookingRejected.addSeries(Queue);
		datasetBookingRejected.addSeries(RejectedPassenger);
		
		
		this.sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION); 
		this.log.logQREntries(Constants.DURATION, sim);
		this.log.finalise(this.sim);
		output += "\n" + "End of Simulation\n";
		output += sim.finalState();
		
	}
	
	
	private  JFreeChart createBookingChart(final XYDataset dataset) {
        final JFreeChart result = ChartFactory.createTimeSeriesChart(
                "Chart 1: Bookings", "Days", "Passengers", dataset, true, true, false);
            final XYPlot plot = result.getXYPlot();
            //Setting Colors for the Plotted lines 
            //it follows createTimeSeriesData collection
            plot.getRenderer().setSeriesPaint(0, Color.black); 
            plot.getRenderer().setSeriesPaint(1, Color.blue);
            plot.getRenderer().setSeriesPaint(2, Color.cyan);
            plot.getRenderer().setSeriesPaint(3, Color.gray);
            plot.getRenderer().setSeriesPaint(4, Color.green);
            plot.getRenderer().setSeriesPaint(5, Color.red);
            
            ValueAxis domain = plot.getDomainAxis();
            domain.setAutoRange(true);
            ValueAxis range = plot.getRangeAxis();
            range.setAutoRange(true);
            return result;
	}
	
	private JFreeChart createBookRefusedChart (final XYDataset datasetBookingRejected){
		final JFreeChart result = ChartFactory.createTimeSeriesChart(
                "Chart 2: Summary", "Days", "Passengers", datasetBookingRejected, true, true, false);
        final XYPlot plot = result.getXYPlot();
        //Setting Colors for the Plotted lines 
        //it follows createTimeSeriesData collection
        plot.getRenderer().setSeriesPaint(0, Color.black); //Queue
        plot.getRenderer().setSeriesPaint(1, Color.red);	//Rejected
        ValueAxis domain = plot.getDomainAxis();
        domain.setAutoRange(true);
        ValueAxis range = plot.getRangeAxis();
        range.setAutoRange(true);
        return result;
	}
	
}
