package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

public class FirstTests {

	Passenger firstNub;
	
	@Before
	public void setUp() throws PassengerException{
		firstNub = new First(1,3);
		firstNub.queuePassenger(1,3);
	}
	
	@Test (expected = PassengerException.class)
	public void bookingTimeLessThanZero() throws PassengerException {
		Passenger firstNub = new First(-1,3);
	}
	
	@Test (expected = PassengerException.class)
	public void departureTimeTimeLessThanZero() throws PassengerException {
		Passenger firstNub = new First(0,-1);
	}
	
	@Test (expected = PassengerException.class)
	public void departureTimeLessThanBookingTime() throws PassengerException {
		Passenger firstNub = new First(4,3);
	}
	
	@Test (expected = PassengerException.class)
	public void queueTimeLessThanZero() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(-1, 4);
	}

	@Test (expected = PassengerException.class)
	public void departureTimeLessThanQueueTime() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(5, 4);
	}
	
	@Test 
	public void passengerIsInQueuedState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		assertTrue(firstNub.isQueued());
	}
	
	@Test (expected = PassengerException.class)
	public void confirmationTimeLessThanZero() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.confirmSeat(-1, 4);
	}
	
	@Test (expected = PassengerException.class)
	public void departureTimeLessThanConfirmationTime() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.confirmSeat(5, 4);
	}
	
	@Test 
	public void checkTheyAreConfirmed() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.confirmSeat(3, 4);
		assertTrue(firstNub.isConfirmed());
	}
	
	@Test (expected = PassengerException.class)
	public void cancelUnconfirmedPassenger() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.cancelSeat(3);
	}
	
	@Test (expected = PassengerException.class)
	public void cancellationTimeLessThanZero() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.cancelSeat(-1);
	}
	
	@Test
	public void successfullyCancelPassenger() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.confirmSeat(3, 4);
		firstNub.cancelSeat(3);
		assertFalse(firstNub.isConfirmed());
		assertFalse(firstNub.isQueued());
	}
	
	@Test
	public void checkBookingTimeChanged() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.confirmSeat(3, 4);
		int testTime = firstNub.getBookingTime();
		firstNub.cancelSeat(3);
		assertEquals(3, testTime);
	}
	
	@Test
	public void flyPassenger() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		firstNub.confirmSeat(3, 4);
		firstNub.flyPassenger(4);
		assertTrue(firstNub.isFlown());
		assertFalse(firstNub.isConfirmed());
	}
	
	@Test 
	public void passengerWasQueuedInQueueState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		assertTrue(firstNub.wasQueued());
	}
	
	@Test 
	public void passengerWasQueuedConfirmedState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		firstNub.confirmSeat(3, 4);
		assertTrue(firstNub.wasQueued());
	}
	
	@Test 
	public void passengerWasQueuedInFlownState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		firstNub.confirmSeat(3, 4);
		firstNub.flyPassenger(4);
		assertTrue(firstNub.wasQueued());
	}
	
	@Test 
	public void passengerWasQueuedInNewState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		assertFalse(firstNub.wasQueued());
	}
	
	@Test 
	public void passengerWasConfirmedInConfirmedState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		firstNub.confirmSeat(3, 4);
		assertTrue(firstNub.wasConfirmed());
	}

	@Test 
	public void passengerWasConfirmedInFlownState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		firstNub.confirmSeat(3, 4);
		firstNub.flyPassenger(4);
		assertTrue(firstNub.wasConfirmed());
	}
	
	@Test 
	public void passengerWasConfirmedInQueueState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		firstNub.queuePassenger(3, 4);
		assertFalse(firstNub.wasConfirmed());
	}

	@Test 
	public void passengerWasConfirmedInNewState() throws PassengerException {
		Passenger firstNub = new First(3,4);
		assertFalse(firstNub.wasConfirmed());
	}
}
