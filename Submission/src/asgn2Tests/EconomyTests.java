package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Passengers.Economy;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

public class EconomyTests {

	@Test 
	public void passengerIsUpgraded() throws PassengerException {
		Passenger firstNub = new Economy(3,4);
		assertEquals("Y:0", firstNub.getPassID());
		firstNub.upgrade();
		assertEquals("P:0", firstNub.getPassID());
		
	}
	
	@Test
	public void testNoSeatsMsg() {
		fail("Not yet implemented");
	}


	@Test
	public void testEconomy() {
		fail("Not yet implemented");
	}
	
	
}
