package asgn2Tests;

import static org.junit.Assert.*;

import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;
import asgn2Simulators.Log;
import org.junit.Before;
import org.junit.Test;

import asgn2Aircraft.A380;
import asgn2Aircraft.Aircraft;
import asgn2Aircraft.AircraftException;
import asgn2Aircraft.Bookings;

public class A380Test {

	@Before
	public void setUp() throws Exception {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
	}
	
	//
	//Tests for the Aircraft Constructor Exception
	//
	
	@Test (expected = AircraftException.class)
	public void testAircraftFlightCodeNull() throws AircraftException {
		Aircraft testAircraftM = new A380(null, 10, 1,1,1,1);
	}
	
	@Test (expected = AircraftException.class)
	public void testAircraftDepartTime0() throws AircraftException {
		Aircraft testAircraftM = new A380("test", 0, 1,1,1,1);
	}
	@Test (expected = AircraftException.class)
	public void testAircraftPE0() throws AircraftException {
		Aircraft testAircraftM = new A380("test", 1, 1,1,1,-1);
	}
	@Test (expected = AircraftException.class)
	public void testAircraftPP0() throws AircraftException {
		Aircraft testAircraftM = new A380("test", 1, 1,1,-1,1);
	}
	@Test (expected = AircraftException.class)
	public void testAircraftPB0() throws AircraftException {
		Aircraft testAircraftM = new A380("test", 1, 1,-1,1,1);
	}
	@Test (expected = AircraftException.class)
	public void testAircraftPF0() throws AircraftException {
		Aircraft testAircraftM = new A380("test", 1, -1,1,1,1);
	}
	
	//
	//Cancel Booking tests
	//
	
	@Test
	public void testCancelBookingWorking() throws PassengerException, AircraftException {
		Passenger p = new First (1,1);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.cancelBooking(p, 1);
		assertFalse(p.isConfirmed());
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelBookingCancelTimeInValid() throws PassengerException, AircraftException {
		Passenger p = new First (1,1);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.cancelBooking(p, 2);;
	}
	
	@Test(expected = PassengerException.class)
	public void testCancelBookingInvalid() throws PassengerException, AircraftException {
		Passenger p = new First (1,1);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.cancelBooking(p, 1);
	}
	
	@Test
	public void testCancelBookingInvalidPlane() throws PassengerException, AircraftException {
		Passenger p = new First (1,2);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.confirmBooking(p,1);
		testAircraft.cancelBooking(p, 1);
		assertFalse(p.isConfirmed());
	}
	
	//
	//Confirm booking test
	//
	
	@Test
	public void testConfirmBookingWorking() throws PassengerException, AircraftException {
		Passenger p = new First (1,2);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.confirmBooking(p,1);
		assertTrue(p.isConfirmed());
	}
	@Test (expected = PassengerException.class)
	public void testConfirmBookingIncorrectState() throws PassengerException, AircraftException {
		Passenger p = new First (1,1);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.confirmBooking(p,1);
		testAircraft.confirmBooking(p,1);
	}
	
	@Test (expected = AircraftException.class)
	public void testConfirmBookingNoSeat() throws PassengerException, AircraftException {
		Passenger p = new First (1,1);
		Passenger p2 = new First (1,1);
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		testAircraft.confirmBooking(p,1);
		testAircraft.confirmBooking(p2, 1);
	}

	//
	//Flight Empty
	//
	
	@Test
	public void testFlightEmptyTrue() throws AircraftException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		assertTrue(testAircraft.flightEmpty());
	}
	
	public void testFlightEmptyFalse() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		assertFalse(testAircraft.flightEmpty());
	}
	
	//
	//Flight full
	//

	@Test
	public void testFlightFullFalse() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		assertFalse(testAircraft.flightFull());
	}
	public void testFlightFullTrue() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,0,0,0);
		Passenger p = new First (1,1);
		assertTrue(testAircraft.flightFull());
	}
	
	//
	//Fly Passengers 
	//

	@Test
	public void testFlyPassengersWorking() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,0,0,0);
		Passenger p = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.flyPassengers(1);
		assertTrue(p.isFlown());
	}

	@Test(expected = PassengerException.class)
	public void testFlyPassengersIncorrectStateFlown() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,0,0,0);
		Passenger p = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.flyPassengers(1);
		testAircraft.flyPassengers(1);
	}
	@Test(expected = PassengerException.class)
	public void testFlyPassengersIncorrectStateRefused() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,0,0,0);
		Passenger p = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		p.refusePassenger(1);
		testAircraft.flyPassengers(1);
	}
	
	@Test(expected = PassengerException.class)
	public void testFlyPassengersIncorrectStateDeparturetime() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,0,0,0);
		Passenger p = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.flyPassengers(0);
	}
	
	//
	//Get Booking time
	//
	@Test
	public void testGetBookings() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		Passenger p2 = new Business (1,1);
		Passenger p3 = new Premium (1,1);
		Passenger p4 = new Economy (1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.confirmBooking(p2, 1);
		testAircraft.confirmBooking(p3, 1);
		testAircraft.confirmBooking(p4, 1);
		Bookings b1 = testAircraft.getBookings();
		assertTrue(b1.getNumFirst() == 1 && b1.getNumBusiness() == 1 && b1.getNumPremium() == 1 && b1.getNumEconomy() ==1);
	}

	//
	//Get Num Business
	//
	@Test
	public void testGetNumBusiness() throws PassengerException, AircraftException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new Business (1,1);
		testAircraft.confirmBooking(p, 1);
		assertTrue(testAircraft.getNumBusiness() ==1);
	}

	//
	//Get Num Economy
	//
	@Test
	public void testGetNumEonomy() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new Economy (1,1);
		testAircraft.confirmBooking(p, 1);
		assertTrue(testAircraft.getNumEonomy() ==1);
	}

	//
	//Get Num First
	//
	@Test
	public void testGetNumFirst() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		assertTrue(testAircraft.getNumFirst() ==1);
	}

	//
	//Get Num Passengers
	//
	@Test
	public void testGetNumPassengers() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		Passenger p2 = new Business (1,1);
		Passenger p3 = new Premium (1,1);
		Passenger p4 = new Economy (1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.confirmBooking(p2, 1);
		testAircraft.confirmBooking(p3, 1);
		testAircraft.confirmBooking(p4, 1);
		assertTrue(testAircraft.getNumPassengers() == 4);
	}

	//
	//Get Num Premium
	//
	@Test
	public void testGetNumPremium() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new Premium (1,1);
		testAircraft.confirmBooking(p, 1);
		assertTrue(testAircraft.getNumPremium() ==1);
		}
	
	//
	//Get Passengers
	//
	@Test
	public void testGetPassengers() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		Passenger p2 = new Business (1,1);
		Passenger p3 = new Premium (1,1);
		Passenger p4 = new Economy (1,1);
		
		testAircraft.confirmBooking(p, 1);
		testAircraft.confirmBooking(p2, 1);
		testAircraft.confirmBooking(p3, 1);
		testAircraft.confirmBooking(p4, 1);
		
		assertTrue(testAircraft.getNumPassengers() == 4);
	}
	


	@Test
	public void testHasPassengerTrue() throws PassengerException, AircraftException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		assertTrue(testAircraft.hasPassenger(p));
	}



	@Test
	public void testSeatsAvailableTrue() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		Passenger p2 = new First (1,1);
		assertTrue(testAircraft.seatsAvailable(p));
	}
	
	@Test
	public void testSeatsAvailableFalse() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new First (1,1);
		Passenger p2 = new First (1,1);
		testAircraft.confirmBooking(p, 1);
		assertFalse(testAircraft.seatsAvailable(p));
	}


	@Test
	public void testUpgradeBookings() throws AircraftException, PassengerException {
		Aircraft testAircraft = new A380("test",1,1,1,1,1);
		Passenger p = new Economy (1,1);
		Passenger p2 = new Economy (1,1);
		testAircraft.confirmBooking(p, 1);
		testAircraft.upgradeBookings();
		assertFalse(testAircraft.seatsAvailable(p));
	}

}
