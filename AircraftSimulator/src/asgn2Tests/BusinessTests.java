package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Passengers.Business;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;

public class BusinessTests {

	@Test 
	public void passengerIsUpgraded() throws PassengerException {
		Passenger firstNub = new Business(3,4);
		assertEquals("J:0", firstNub.getPassID());
		firstNub.upgrade();
		assertEquals("F:0", firstNub.getPassID());
		
	}

}
