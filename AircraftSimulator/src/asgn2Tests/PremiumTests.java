package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

public class PremiumTests {

	@Test 
	public void passengerIsUpgraded() throws PassengerException {
		Passenger firstNub = new Premium(3,4);
		assertEquals("P:0", firstNub.getPassID());
		firstNub.upgrade();
		assertEquals("J:0", firstNub.getPassID());
		
	}
}
